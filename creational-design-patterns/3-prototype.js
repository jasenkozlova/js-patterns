const car = {
    wheels: 4,
    init () {
        console.log(`Owner: ${this.owner}`);
    }
}

carWithOwner = Object.create(car, {owner: {
    value: "Yana"
    }})
console.log(carWithOwner.__proto__ === car);
carWithOwner.init();
