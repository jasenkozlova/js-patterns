class User {
    constructor(name) {
        this.name = name;
        this.role = null
    }
    send(massage, to) {
        this.room.send(massage, this, to)
    }
    receive(massage, from) {
        console.log(`${from.name} => ${this.name}: ${massage}`);
    }
}

class ChatRoom {
    constructor() {
        this.users = {}
    }
    register(user) {
        this.users[user.name] = user;
        user.room = this
    }
    send(massage, from, to) {
        to? to.receive(massage, from)
            : Object.keys(this.users).forEach(key => {
                if (this.users[key] !== from) {
                    this.users[key].receive(massage, from);
                }
            })
    }
}

const yana = new User("Yana");
const artem = new User('Artem');
const andrey = new User('Andrey');

const room = new ChatRoom();

room.register(yana);
room.register(artem);
room.register(andrey);

yana.send("Hello", artem);
artem.send("jckjcnkjxn", yana);
andrey.send("Hello all");
