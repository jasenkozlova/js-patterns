class Employee {
    constructor(name, salary) {
        this.name = name;
        this.salary = salary;
    }
    responsibilities() {}

    work() {
        return `${this.name} do ${this.responsibilities()} `
    }
    getPaid() {
        return `${this.name} gets ${this.salary}`
    }
}

class Developer extends Employee {
    constructor(name, salary) {
        super(name, salary);
    }
    responsibilities() {
        return 'developing'
    }
}

class Tester extends Employee {
    constructor(name, salary) {
        super(name, salary);
    }
    responsibilities() {
        return 'testing'
    }
}

const dev = new Developer("Yana", 10000);
console.log(dev.getPaid());
console.log(dev.work());

const tester = new Tester("Yulia", 8000);
console.log(tester.getPaid());
console.log(tester.work());
